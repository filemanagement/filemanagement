<?php

require './aws-autoloader.php';

use Aws\Credentials\CredentialProvider;
use Aws\Textract\TextractClient;

$client = new TextractClient([
	'region' => 'us-west-2',
	'version' => '2018-06-27',
	'credentials' => [
		'key'    => 'AKIA5UACA4RFIG2XXFVD',
		'secret' => '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'
	]
]);

// The file in this project.
$filename = "a.pdf";
$file = fopen($filename, "rb");
$contents = fread($file, filesize($filename));
fclose($file);
$options = [
	'Document' => [
		'Bytes' => $contents
	],
	'FeatureTypes' => ['TABLES'], ['FORMS']// REQUIRED
];
$promise = $client->analyzeDocumentAsync($options);
$promise->then(
	// $onFulfilled
	function ($value) {
		echo 'The promise was fulfilled.';
		processResult($value);
	},
	// $onRejected
	function ($reason) {
		echo 'The promise was rejected.';
	}
);

// If debugging:
// echo print_r($result, true);
function processResult($result)
{
	$blocks = $result['Blocks'];
	// Loop through all the blocks:
	foreach ($blocks as $key => $value) {
		if (isset($value['BlockType']) && $value['BlockType'] && $value['BlockType']) {
			$blockType = $value['BlockType'];
			if (isset($value['Text']) && $value['Text'] && $value['Text']) {
				$text = $value['Text'];
				if ($blockType == 'WORD') {
					echo "Word: " . print_r($text, true) . "\n";
				} else if ($blockType == 'LINE') {
					echo "Line: " . print_r($text, true) . "\n";
				} else if ($blockType == 'TABLE') {
					echo "Table: " . print_r($text, true) . "\n";
			}
		}
	}