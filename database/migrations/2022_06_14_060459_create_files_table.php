<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_unit');
            $table->bigInteger('id_folder');
            $table->bigInteger('id_aplikasi_existing');
            $table->bigInteger('id_pengguna');
            $table->bigInteger('id_jenis_file');
            $table->string('nama_file', 100);
            $table->dateTime('tanggal_dibuat');
            $table->dateTime('tanggal_dihapus');
            $table->dateTime('tanggal_diupdate');
            $table->string('versi', 11);
            $table->double('ukuran');
            $table->enum('sumber', ['user_upload', 'aplikasi_existing']);
            $table->enum('status', ['publik', 'privat']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
