<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatVersisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_versis', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_file');
            $table->string('versi', 11);
            $table->dateTime('tanggal_dibuat');
            $table->dateTime('tanggal_diupdate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_versis');
    }
}
