<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class unit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            'name' => 'Dinas Komunikasi, Informatika, dan Statistik',
            'id_folder' => 1,
            'id_parent_unit' => 1,
            'id_unit_opd' => 1
        ]);
    }
}
