<?php 

return [
    'token' => ENV('PICASO_TOKEN'),
    'url' => ENV('PICASO_URL')
];