<?php

use Illuminate\Support\Facades\Route;
use App\Services\Ocr\PicasoOcrService;

Route::get('/picaso_pdf_ocr', function () {
    $PicasoOcrService = new PicasoOcrService;
    $file_path = 'files/test_file.pdf';
    return $PicasoOcrService->pdfTextExtract($file_path);
});
