<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OperatorController;
use Illuminate\Http\Request;
use App\Http\Controllers\BlogController;

//Namespace Auth
use App\Http\Controllers\Auth\LoginController;

//Namespace Admin
use App\Http\Controllers\Admin\AdminController;

//Namespace User
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\ProfileController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*SSO*/
Route::get('backend/login','SSOBrokerController@authenticateToSSO');
Route::get('authenticateToSSO','SSOBrokerController@authenticateToSSO');
Route::get('authData/{authData}','SSOBrokerController@authenticateToSSO');
Route::get('logout/{sessionId}','SSOBrokerController@logout');
Route::get('changeRole/{role}', 'SSOBrokerController@changeRole')->name('changeRole');

Route::group(['middleware' => ['SSOBrokerMiddleware']], function () {
    Route::get('test', function(){
       return 'test';
    });
 });
 
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home',[FileUploadController::class, 'index']);
Route::post('upload',[FileUploadController::class, 'upload'])->name('file.upload');

/*Sidebar*/
Route::get('/dashboard',[OperatorController::class, 'dashboard']);
Route::resource('/operator',OperatorController::class);
Route::get('/fileopd',[OperatorController::class, 'fileopd']);
Route::get('/filesaya',[OperatorController::class, 'filesaya']);
Route::get('/manajemenaplikasi',[OperatorController::class, 'manajemenaplikasi']);
Route::get('/temukandokumen',[OperatorController::class, 'temukandokumen']);


Route::group(['namespace' => 'User','middleware' => 'auth' ,'prefix' => 'user'],function(){
	Route::get('/',[UserController::class,'index'])->name('user');
	Route::get('/profile',[ProfileController::class,'index'])->name('profile');
	Route::patch('/profile/update/{user}',[ProfileController::class,'update'])->name('profile.update');
});

Route::group(['namespace' => 'Auth','middleware' => 'guest'],function(){
	Route::view('/login','auth.login')->name('login');
	Route::post('/login',[LoginController::class,'authenticate'])->name('login.post');
});

// Other
Route::view('/register','auth.register')->name('register');
Route::view('/forgot-password','auth.forgot-password')->name('forgot-password');
Route::post('/logout',function(){
	return redirect()->to('/login')->with(Auth::logout());
})->name('logout');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
