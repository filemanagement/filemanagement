<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form Upload File</title>
    <link rel="stylesheet" href="{{ asset('styles/css/bootstrap.min.css') }}"> 
</head>
<body>

    <div class="container mt-5">
    <div class="row" style="margin-top:45px">
        <div class="col-md-4 col-md-offset-4">
            <h4>File Upload</h4>
            <form action="{{ route('file.upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="_file" id="_file" aria-label="File browser example"><br>
                <span class="file-custom"></span>
                <button type="submit" class="btn btn-success">Upload</button>
            </form>
        </div>
    </div>
    </div>
    </body>

</html>