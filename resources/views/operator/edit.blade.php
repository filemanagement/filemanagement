<div class="modal fade" id="m_operator_edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Operator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Input gagal.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
            <form action="{{ route('operator.update',$user->id) }}" method="POST" id="editForm">
                @csrf
                @method('PUT')

                <div class="column">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label class="font-weight-bold">Nama</label>
                            <input type="text" name="nama" class="form-control" id="nama">
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Alamat Email</label>
                                <input type="email" name="email" class="form-control" id="email">
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">OPD</label>
                                <select class="form-control" name="id_unit" id="id_unit">
                                    @foreach ($data_units as $unit)
                                    <option value="{{$unit->id}}">{{$unit->nama}}</option>
                                    @endforeach
                                    <!-- <option value="Dinas Komunikasi, Informatika dan Statistik" {{ old('created_unit_id')=='Dinas Komunikasi, Informatika dan Statistik' ? 'selected': '' }}>Dinas Komunikasi, Informatika dan Statistik</option>
                                    <option>Dinas Kesehatan</option>
                                    <option>Dinas Pendidikan, Kepemudaan dan Olahraga</option>
                                    <option>Dinas Perhubungan</option>
                                    <option>Dinas Kehutanan dan Lingkungan Hidup</option>
                                    <option>Dinas Perindustrian dan Perdagangan</option> -->
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Role</label>
                                <select class="form-control" name="role" id="role">
                                    <option value="Admin" {{ old('role')=='Admin' ? 'selected': '' }}>Admin</option>
                                    <option value="User" {{ old('role')=='User' ? 'selected': '' }}>User</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="Aktif" {{ old('status')=='Aktif' ? 'selected': '' }}>Aktif</option>
                                    <option value="Nonaktif" {{ old('status')=='Nonaktif' ? 'selected': '' }}>Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-md btn-warning">Reset</button>
                        <button type="submit" id="saveModalButton" class="btn btn-md btn-primary">Tambah</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>