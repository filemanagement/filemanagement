<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <title>File Management System</title>
    @include('template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
                <div class="hero">
                    <img src="{{('template/img/LOGO MENFESS 1.svg')}}" width="50" height="500">
                </div>
                <div class="sidebar-brand-text mx">MenfeSs</div>
            </a>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item ">
                <a class="nav-link" href="dashboard">
                    <img src="img/Health Data.svg" width="22">
                    <span>Dashboard<span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="operator">
                    <img src="img/Engineering.svg" width="22">
                    <span>Operator<span></a>
            </li>
            <!-- Nav Item - Pages Collapse Menu -->

            <!-- Nav Item - Utilities Collapse Menu -->


            <!-- Divider -->

            <li class="nav-item ">
                <a class="nav-link" href="fileopd">
                    <img src="img/New Document.svg" width="22">
                    <span>File OPD<span></a>
            </li>

            <!-- Nav Item - Pages Collapse Menu
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>File Saya</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="login.html">Login</a>
                        <a class="collapse-item" href="register.html">Register</a>
                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="404.html">404 Page</a>
                        <a class="collapse-item" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li> -->

            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="filesaya">
                    <img src="img/Document.svg" width="22">
                    <span>File Saya</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="manajemenaplikasi">
                    <img src="img/Database View.svg" width="22">
                    <span>Manajemen Aplikasi</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="temukandokumen">
                    <img src="img/Selling Strategy Document.svg" width="22">
                    <span>Temukan Dokumen</span></a>
            </li>


            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->

            <!-- Topbar -->
            @include('template.navbar')
            <!-- End of Topbar -->

            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Operator</h1>

                <!-- Add Modal -->
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#m_operator" onclick="createModal('create');" data-method="create" data-whatever="@mdo" id="create">Tambah Operator</button>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="myTable" width="100%" cellspacing="0">
                            <thead>
                                <tr w3-light-grey>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Alamat Email</th>
                                    <th>OPD</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th width="200px" class="text-center">Aksi</th>
                                </tr>
                            <tbody>
                                @foreach ($data_users as $i => $user)
                                <tr>
                                    <td>{{ $i + 1 }}</td>
                                    <td>{{ $user->nama }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->createdUnit->nama}}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>{{ $user->status }}</td>
                                    <td class="text-center">
                                        <!-- Show Modal -->
                                        <button type="button" class="btn btn-info btn-sm" onclick="showModal('show');" data-toggle="modal" data-target="#m_operator_show" data-method="show" data-nama="{{ $user->nama }}" data-email="{{ $user->email }}" data-unit_nama="{{ $user->createdUnit->nama }}" data-id_unit="{{ $user->id_unit }}" data-role="{{ $user->role }}" data-status="{{ $user->status }}">Show</button>

                                        <!-- Edit Modal -->
                                        <button type="button" class="btn btn-primary btn-sm" onclick="editModal('edit');" data-toggle="modal" data-target="#m_operator_edit" data-method="edit" data-nama="{{ $user->nama }}" data-email="{{ $user->email }}" data-unit_nama="{{ $user->createdUnit->nama }}" data-id_unit="{{ $user->id_unit }}" data-role="{{ $user->role }}" data-status="{{ $user->status }}">Edit</button>

                                        <!-- Hapus Modal-->
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="hapusModal('dispose');" data-toggle="modal" data-target="#m_operator_hapus" data-method="edit" data-nama="{{ $user->nama }}" data-email="{{ $user->email }}" data-unit_nama="{{ $user->createdUnit->nama }}" data-id_unit="{{ $user->id_unit }}" data-role="{{ $user->role }}" data-status="{{ $user->status }}">Hapus</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- End of Content Wrapper -->


            <!-- End of Page Wrapper -->


            <!-- Footer -->
            @include('template.footer')
            <!-- End of Footer -->



            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current
                        session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        @include('operator.create')
        @include('operator.show')
        @include('operator.edit')
        @include('operator.delete')


        <!-- Bootstrap core JavaScript-->
        @include('template.script')

</body>

<script>
    $(document).ready(function() {
        $('.hapusModal').click(function(e) {
            e.preventDefault();

            var id = $(this).val();
            $('#m_operator_hapus').modal('dispose');
        })
    })

    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    function showModal(action) {
        $('#m_operator_show').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var nama = button.data('nama')
            var email = button.data('email')
            var id_unit = button.data('id_unit')
            var unit_nama = button.data('unit_nama')
            var role = button.data('role')
            var status = button.data('status')

            $('#nama').val(nama)
            $('#email').val(email)
            $('#m_operator_show #id_unit').val(unit_nama)
            $('#m_operator_show #role').val(role)
            $('#m_operator_show #status').val(status)

            console.log(nama);
            console.log(email);
            console.log(id_unit);
            console.log(role);
            console.log(status);
        })
    }

    function editModal(action) {
        $('#m_operator_edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var nama = button.data('nama')
            var email = button.data('email')
            var id_unit = button.data('id_unit')
            var unit_nama = button.data('unit_nama')
            var role = button.data('role')
            var status = button.data('status')

            $('#m_operator_edit #nama').val(nama)
            $('#m_operator_edit #email').val(email)
            $('#m_operator_edit #id_unit').val(id_unit)
            $('#m_operator_edit #unit_nama').val(unit_nama)
            $('#m_operator_edit #role').val(role)
            $('#m_operator_edit #status').val(status)

            console.log(nama);
            console.log(email);
            console.log(id_unit);
            console.log(unit_nama);
            console.log(role);
            console.log(status);
        })
    }

</script>

</html>