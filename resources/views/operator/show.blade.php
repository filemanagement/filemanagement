<div class="modal fade" id="m_operator_show" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Show Operator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Input gagal.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

                <div class="column">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label class="font-weight-bold">Nama</label>
                            <input type="text" name="nama" class="form-control" id="nama" readonly>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Alamat Email</label>
                                <input type="email" name="email" class="form-control" id="email" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">OPD</label>
                                <input type="text" name="id_unit" class="form-control" id="id_unit" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Role</label>
                                <input type="text" name="role" class="form-control" id="role" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Status</label>
                                <input type="text" name="status" class="form-control" id="status" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>