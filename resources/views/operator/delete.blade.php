<div class="modal fade" id="m_operator_hapus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form action="{{ route('operator.destroy',$user->id) }}" method="POST">
                @csrf
                @method('DELETE')
                
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Operator</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="$user->id" id="$user->id">
        <h5>Apakah Anda Yakin Ingin Menghapus ?</h5>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Ya</button>
      </div>
    </div>
  </div>
</div>