<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <title>File Management System</title>
    @include('template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
                <div class="hero">
                    <img src="{{('template/img/LOGO MENFESS 1.svg')}}" width="50" height="500">
                </div>
                <div class="sidebar-brand-text mx">MenfeSs</div>
            </a>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="dashboard">
                    <img src="img/Health Data.svg" width="22">
                    <span>Dashboard<span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="operator">
                <img src="img/Engineering.svg" width="22">
                    <span>Operator<span></a>
            </li>
            <!-- Nav Item - Pages Collapse Menu -->

            <!-- Nav Item - Utilities Collapse Menu -->


            <!-- Divider -->

            <li class="nav-item ">
                <a class="nav-link" href="fileopd">
                <img src="img/New Document.svg" width="22">
                    <span>File OPD<span></a>
            </li>

            <!-- Nav Item - Pages Collapse Menu
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>File Saya</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="login.html">Login</a>
                        <a class="collapse-item" href="register.html">Register</a>
                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="404.html">404 Page</a>
                        <a class="collapse-item" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li> -->

            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="filesaya">
                <img src="img/Document.svg" width="22">
                    <span>File Saya</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="manajemenaplikasi">
                <img src="img/Database View.svg" width="22">
                    <span>Manajemen Aplikasi</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="temukandokumen">
                <img src="img/Selling Strategy Document.svg" width="22">
                    <span>Temukan Dokumen</span></a>
            </li>


            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div class="">
                @yield('content')
            </div>
            <!-- Topbar -->
            @include('template.navbar')
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Beranda</h1>
                </div>
                <p class="h3 mb-0 text-gray-800">Hello Cameron Williamson, Selamat datang!</p>

                <div class="row">

                    <!-- Earnings Dokumen -->
                    <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Dokumen</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">123</div>
                                        </div>
                                        <div class="col-auto">
                                            <img src="img/Opened Folder.svg" width="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings video -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Video</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">123</div>
                                        </div>
                                        <div class="col-auto">
                                            <img src="img/Video Playlist.svg" width="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PDF -->
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">PDF
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">123</div>
                                                </div>
                                                <div class="col">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <img src="img/PDF.svg" width="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                    <p class="text-dark"><strong>Berkas Terbaru</strong></p>
                        <!-- Content Row -->
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="myTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Pemilik</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Terakhir diubah</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                            <th scope="row"><img src="img/ant-design_file-pdf-filled.svg" width="50">
                                                Layout</th>
                                            <td>Jame Cooper</td>
                                            <td>1.5 GB</td>
                                            <td>28/10/2022</td>
                                            <td><img src="img/More.svg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="" alt=""><img
                                                    src="img/ant-design_file-pdf-filled.svg" width="50"> Project</th>
                                            <td>Wade Warren</td>
                                            <td>1.5 GB</td>
                                            <td>21/12/2021</td>
                                            <td><img src="img/More.svg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/ant-design_file-pdf-filled.svg" width="50">
                                                Side-out</th>
                                            <td>Wade Warren</td>
                                            <td>1.5 GB</td>
                                            <td>16/05/2020</td>
                                            <td><img src="img/More.svg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/ant-design_file-pdf-filled.svg"
                                                    width="50">Planning</th>
                                            <td>Jenmy Willson</td>
                                            <td>1.5 GB</td>
                                            <td>09/19/2020</td>
                                            <td><img src="img/More.svg" alt=""></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/ant-design_file-pdf-filled.svg"
                                                    width="50">Move</th>
                                            <td>Robert Fox</td>
                                            <td>1.5 GB</td>
                                            <td>01/10/2019</td>
                                            <td><img src="img/More.svg" alt=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- Content Row -->

                        <div class="row"></div>

                        <!-- Content Row -->
                        <div class="row">

                            <!-- Content Column -->
                            <div class="col-lg-6 mb-4">
                            </div>
                        </div>

                        <!-- Color System -->
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                            </div>

                        </div>



                        <!-- Illustrations -->


                        <!-- Footer -->
                        @include('template.footer')
                        <!-- End of Footer -->

                    </div>
                    <!-- End of Content Wrapper -->

                </div>
                <!-- End of Page Wrapper -->

                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fas fa-angle-up"></i>
                </a>

                <!-- Logout Modal-->
                <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">Select "Logout" below if you are ready to end your current
                                session.</div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <a class="btn btn-primary" href="login.html">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Bootstrap core JavaScript-->
                @include('template.script')

</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>

</html>