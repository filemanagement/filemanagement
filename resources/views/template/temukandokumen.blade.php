<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <title>File Management System</title>
    @include('template.head')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
                <div class="hero">
                    <img src="{{('template/img/LOGO MENFESS 1.svg')}}" width="50" height="500">
                </div>
                <div class="sidebar-brand-text mx">MenfeSs</div>
            </a>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item ">
                <a class="nav-link" href="dashboard">
                <img src="img/Health Data.svg" width="22">
                    <span>Dashboard<span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="operator">
                <img src="img/Engineering.svg" width="22">
                    <span>Operator<span></a>
            </li>
            <!-- Nav Item - Pages Collapse Menu -->

            <!-- Nav Item - Utilities Collapse Menu -->


            <!-- Divider -->

            <li class="nav-item ">
                <a class="nav-link" href="fileopd">
                <img src="img/New Document.svg" width="22">
                    <span>File OPD<span></a>
            </li>

            <!-- Nav Item - Pages Collapse Menu
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>File Saya</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="login.html">Login</a>
                        <a class="collapse-item" href="register.html">Register</a>
                        <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                        <div class="collapse-divider"></div>
                        <h6 class="collapse-header">Other Pages:</h6>
                        <a class="collapse-item" href="404.html">404 Page</a>
                        <a class="collapse-item" href="blank.html">Blank Page</a>
                    </div>
                </div>
            </li> -->

            <!-- Nav Item - Charts -->
            <li class="nav-item">
                <a class="nav-link" href="filesaya">
                <img src="img/Document.svg" width="22">
                    <span>File Saya</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="manajemenaplikasi">
                <img src="img/Database View.svg" width="22">
                    <span>Manajemen Aplikasi</span></a>
            </li>

            <!-- Nav Item - Tables -->
            <li class="nav-item active">
                <a class="nav-link" href="temukandokumen">
                <img src="img/Selling Strategy Document.svg" width="22">
                    <span>Temukan Dokumen</span></a>
            </li>


            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('template.navbar')
                <!-- ENd Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Tambah Operator</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Nama</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1"
                                                placeholder>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">OPD</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>Dinas Komunikasi, Informasi dan Statistik</option>
                                                <option>Dinas Kesehatan</option>
                                                <option>Dinas Pendidikan, Kepemudaan dan Olahraga</option>
                                                <option>Dinas Perhubungan</option>
                                                <option>Dinas Kehutanan dan Lingkungan Hidup</option>
                                                <option>Dinas Perindustrian dan Perdagangan</option>
                                            </select>
                                            <div class="form-group"></div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Alamat Email</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Password</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Username</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">NIP</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">No Telephone</label>
                                            <input type="email" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Temukan Dokumen</h1>
                    <p> Disarankan</p>

                    <!-- DataTales Example -->
                    <div class="col-lg-12 mb-4">

                        <!-- Project Card Example -->


                        <!-- Color System -->


                        <div class="col-lg-3 mb-4" style="float:left;">

                            <!-- Illustrations -->
                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                            src="{{('template/img/folder-directory-files.svg')}}" alt="...">
                                    </div>
                                    <a target="_blank" rel="nofollow">USER</a>
                                    <div class="text-center">

                                    </div>


                                </div>

                            </div>


                        </div>
                        <div class="col-lg-3 mb-4" style="float:left;">

                            <!-- Illustrations -->
                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                            src="{{('template/img/folder-directory-files.svg')}}" alt="...">
                                    </div>
                                    <a target="_blank" rel="nofollow">USER</a>
                                    <div class="text-center">

                                    </div>


                                </div>

                            </div>


                        </div>
                        <div class="col-lg-3 mb-4" style="float:left;">

                            <!-- Illustrations -->
                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                            src="{{('template/img/folder-directory-files.svg')}}" alt="...">
                                    </div>
                                    <a target="_blank" rel="nofollow">USER</a>
                                    <div class="text-center">

                                    </div>

                                </div>

                            </div>


                        </div>
                        <div class="col-lg-3 mb-4" style="float:left;">

                            <!-- Illustrations -->
                            <div class="card shadow mb-4">

                                <div class="card-body">
                                    <div class="text-center">
                                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                                            src="{{('template/img/folder-directory-files.svg')}}" alt="...">
                                    </div>
                                    <a target="_blank" rel="nofollow">USER</a>
                                    <div class="text-center">

                                    </div>

                                </div>

                            </div>


                        </div>








                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

       
        <!-- End of Content Wrapper -->

    <!-- End of Page Wrapper -->

    <!-- Footer-->
    @include('template.footer')
     <!-- ENd of footer-->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

   

    @include('template.script')
</body>

</html>