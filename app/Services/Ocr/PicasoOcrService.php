<?php 

namespace App\Services\Ocr;

use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;


class PicasoOcrService 
{
    public function pdfTextExtract($file_path)
    {
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '.Config::get('picaso.token'),
            // 'webhook_url' => route('apiv1.picaso.handle_callback'),
            'webhook_url' => 'https://webhook.site/248718e6-3cbd-4c6e-b19e-ba71aa3ad420',
            'Accept' => 'application/json',
        ];
        $url = Config::get('picaso.url').'/api/order/v1.2/ocr';
        $res = $client->request('POST', $url, [
            'headers' => $headers,
            'http_errors' => false,
            'defaults' => [
                'verify' => false,
            ],
            'multipart' => [
                [
                    'name' => 'pdf',
                    'contents' => Storage::disk('local')->get($file_path),
                    'filename' => basename($file_path),
                ]
            ]
        ]);
        if ($res->getStatusCode() == 200) {
            $body_response = json_decode($res->getBody(), true);
            return [
                'success' => true,
                'message' => $body_response['message'],
                'transaction_id' => $body_response['transaction_id']
            ];
        }
        return [
            'success' => false,
            'message' => $body_response['message'],
            'transaction_id' => null
        ];
    }
}