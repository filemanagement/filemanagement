<?php

namespace App\Services;

use App\Models\User;

class OperatorService
{
    public function getData()
    {
        return User::with('createdUnit')->get();
    }

    public function show($id)
    {
        $users = User::find($id);
        return User::with('createdUnit')->get();
    }	
    
    public function find($user_id)
    {
        return User::with('createdUnit')->find($user_id);
    }

    public function create($data = [])
    {
        return User::create([
            'nama' => $data['nama'],
            'email' => $data['email'],
            'id_unit' => $data['id_unit'],
            'password' => $data['password'],
            'role' => $data['role'],
            'status' => $data['status']
        ]);
    }

    public function update($data = [])
    {
        return User::update([
            'nama' => $data['nama'],
            'email' => $data['email'],
            'id_unit' => $data['id_unit'],
            'role' => $data['role'],
            'status' => $data['status']
        ]);
    }

    public function delete($id)
    {
        $users = User::find($id);
        $users->delete();
    }	

}
