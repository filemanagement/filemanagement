<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\Unit;
use Aws\Textract\TextractClient;
use App\Services\OperatorService;
use App\Models\User;
use Storage;
use Illuminate\Support\Facades\Hash;

class OperatorController extends Controller
{
    protected $OperatorService;
    function __construct(
        OperatorService $OperatorService
    ) {
        $this->OperatorService = $OperatorService;
        
    }

    public function index()
    {
        $users = $this->OperatorService->getData();
        $units = Unit::all();
        //dd($units);

        return view('operator.index', [
            'data_users' => $users,
            'data_units' => $units
        ]);
    }

    public function create()
    {
        return view('operator.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama'     => 'required',
            'email'   => 'required',
            'id_unit' => 'required',
            'password' => 'required',
            'role' => 'required',
            'status'     => 'required'
        ]);

        $user = User::create([
            'nama'     => $request->nama,
            'email'   => $request->email,
            'id_unit'   => $request->id_unit,
            'password' => md5($request->password),
            'role' => $request->role,
            'status'     => $request->status
        ]);

        return redirect()->route('operator.index')->with('Sukses', 'Data Berhasil Disimpan');
    }

    public function show(User $user)
    {
        return view('operator.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('operator.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'nama'     => 'required',
            'email'   => 'required',
            'id_unit' => 'required',
            'role' => 'required',
            'status'     => 'required'
        ]);

        //get data User by ID
        $user = User::findOrFail($user->id);
        $user->update([
            'nama'     => $request->nama,
            'email'   => $request->email,
            'id_unit' => $$request->id_unit,
            'role' => $request->role,
            'status'     => $request->status
        ]);
        return redirect()->route('operator.index')->with('Sukses', 'Data Berhasil Diupdate');
            
    }
    
    public function destroy(User $user)
    {
        $user->delete($user);
        return redirect()->route('operator.index')->with('Sukses', 'Data Berhasil Dihapus');
    }

    public function textract()
    {
        $client = new TextractClient([
            'region' => 'us-west-2',
            'version' => '2018-06-27',
            'credentials' => [
                'key'    => 'AKIA5UACA4RFIG2XXFVD',
                'secret' => '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'
            ]
        ]);

        // The file in this project.
        $filename = "a.pdf";
        $file = fopen($filename, "rb");
        $contents = fread($file, filesize($filename));
        fclose($file);
        $options = [
            'Document' => [
                'Bytes' => $contents
            ],
            'FeatureTypes' => ['TABLES'], ['FORMS'] // REQUIRED
        ];
        $result = $client->analyzeDocument($options);
        // If debugging:
        // echo print_r($result, true);
        $blocks = $result['Blocks'];
        // Loop through all the blocks:
        foreach ($blocks as $key => $value) {
            if (isset($value['BlockType']) && $value['BlockType'] && $value['BlockType']) {
                $blockType = $value['BlockType'];
                if (isset($value['Text']) && $value['Text'] && $value['Text']) {
                    $text = $value['Text'];
                    if ($blockType == 'WORD') {
                        echo "Word: " . print_r($text, true) . "\n";
                    } else if ($blockType == 'LINE') {
                        echo "Line: " . print_r($text, true) . "\n";
                    } else if ($blockType == 'TABLE') {
                        echo "Table: " . print_r($text, true) . "\n";
                    }
                }
            }
        }
    }

    public function dashboard()
    {
        return view('template.dashboard');
    }

    public function operator()
    {
        return view('operator.index');
    }

    public function fileopd()
    {
        return view('template.fileopd');
    }

    public function filesaya()
    {
        return view('template.filesaya');
    }

    public function manajemenaplikasi()
    {
        return view('template.manajemenaplikasi');
    }

    public function temukandokumen()
    {
        return view('template.temukandokumen');
    }
}
