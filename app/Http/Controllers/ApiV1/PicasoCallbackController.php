<?php

namespace App\Http\Controllers\ApiV1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PicasoCallbackController extends Controller
{
    protected $PicasoOcrService;
    function __construct(
        PicasoOcrService $PicasoOcrService
    ) {
        $this->PicasoOcrService = $PicasoOcrService;
        
    }

    public function handleCallback(Request $request)
    {
        
    }
}
