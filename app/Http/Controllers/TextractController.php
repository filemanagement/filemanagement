<?php

namespace App\Http\Controllers;
use App\Services\TextractService;
use Illuminate\Http\Request;

class TextractController extends Controller
{
    protected $TextractService;
    function __construct(
        TextractService $TextractService
    ) {
        $this->TextractService = $TextractService;
    }

    public function index()
    {
        $textracts = $this->TextractService->getData();
        return view('textract.index', [
            'data_textracts' => $textracts
        ]);
    }

    public function create()
    {
        return view('textract.create');
    }

}
