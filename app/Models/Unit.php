<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;
    protected $guarded = [
        'id','created_at','updated_at'
    ];
    
    protected $fillable = [
        'nama', 'id_folder', 'id_parent_unit', 'id_unit_opd'
    ];
}

